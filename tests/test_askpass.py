import pathlib2,os
from context import package as p

p.Pinentry.program = pathlib2.Path(__file__).parent / 'pinentry'

def test_askpass_workflow(seed):
    os.environ['SEED'] = str(seed)
    with p.AskPass() as ap:
        for pwd in ap:
            if pwd[0] == 'a': break
        else:
            pass
