import pathlib2,os
from context import package as p
import pytest

p.Pinentry.program = pathlib2.Path(__file__).parent / 'pinentry'

def test_pinentry_hello():
    with p.Pinentry() as pin:
        pin.nop()

def test_pinentry_workflow(seed):
    os.environ['SEED'] = str(seed)
    with p.Pinentry() as pin:
        out = pin.getpin()
        assert (out is None) or isinstance(out,str)
        pin.reset()
        pin.help()
        pin.setdesc('')
        pin.setprompt('')
        pin.setrepeat()
        pin.setrepeaterror('')
        pin.seterror('')
        pin.setok('')
        pin.setcancel('')
        pin.settimeout(42)
        assert isinstance(pin.confirm(),bool)
        assert pin.message()

def test_pinentry_errors():
    with p.Pinentry(Werror=True) as pin:
        with pytest.raises(p.pinentry.PinentryError,match=r'^testerror$'):
            pin.error('testerror')
        with pytest.raises(ValueError,match='simultaneously'):
            pin._write('string',42,foo=42)
        pin._write('HELP')
        with pytest.raises(p.pinentry.PinentryWarning,match=r'^Unexpected status OK$'):
            stat,msg = pin._read('ERR')
        with pytest.raises(p.pinentry.PinentryError,match=r'^Unknown command$'):
            pin._ok_command('FOO')
